# Contributors Plugin 
##### Wordpress Contributors Plugin

### About Plugin 
It is a Wordpress Plugin for making meta box into post edit page contain checkbox list of all system authors (post main author can check more than one from authors list), In frontend side there is a content filter that add post author and contributors list after the post content in the post single page .

### Installation Guide 
**Clone** this repo into folder **Named** "contributor-plugin" located into website plugins folder, Go to website admin dashboard > Plugins and **Activate** Plugin .

## Enjoy

#### About Author 
- Name : Karim Omar
- Email : karim.omar92@gmail.com
- Website : [KarimOmar.me](http://karimomar.me)