<?php
/*
Plugin Name: Contributor Plugin
Plugin URI: http://www.1234.com
Description: Plugin for making meta box in post edit page checkbox list of all authors that post main author can check for more than one from users as post contributors .
Author: Karim Omar
Version: 1.0
Author URI: http://www.karimomar.me/
*/

// Require All Backend Dependencies
$dependencies = array(
    'backend/contributor-metabox.php',
    'frontend/content-filter.php',
    'includes/frontend/enqueue/enqueue.php',
    'includes/backend/enqueue/enqueue.php'

);

foreach ($dependencies as $dependency):
    include $dependency;
endforeach;