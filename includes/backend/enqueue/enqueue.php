<?php
/*
 * Scripts and Styles enqueue admin dashboard
 */
function co_admin_dashboard_custom_enqueue()
{
    //Register Style
    wp_register_style('co_admin_dashboard_style', plugins_url() . '/contributor-plugin/includes/assets/css/co-admin-style.css');

    //Enqueue Styles
    wp_enqueue_style('co_admin_dashboard_style');

}

add_action('admin_enqueue_scripts', 'co_admin_dashboard_custom_enqueue');