<?php
/*
 * Scripts and Styles enqueue Frontend
 */
function co_frontend_custom_enqueue()
{
    //Register Style and Script
    wp_register_style('contributor_style_css', plugins_url() . '/contributor-plugin/includes/assets/css/contributor-style.css');

    //Enqueue Styles and Scripts
    wp_enqueue_style('contributor_style_css');

}

add_action('wp_enqueue_scripts', 'co_frontend_custom_enqueue');